import {Injectable} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {ClientPartialState} from "./client.reducer";
import {FetchClients, FetchCountries, GetClient, RefreshClient, SaveClient, UpdateClient} from "./client.actions";
import {getClient, getClients, getCountries, getLoaded} from "./client.selectors";
import {Client} from "@test-assignment-web/api";

@Injectable({
  providedIn: 'root'
})
export class ClientFacede {

  client$ = this.store.pipe(select(getClient));
  clients$ = this.store.pipe(select(getClients));
  loaded$ = this.store.pipe(select(getLoaded));
  countries$ = this.store.pipe(select(getCountries));

  constructor(private store: Store<ClientPartialState>) {}

  fetchClient(id: number) {
    this.store.dispatch(new GetClient(id));
  }

  refreshClient() {
    this.store.dispatch(new RefreshClient());
  }

  fetchClients() {
    this.store.dispatch(new FetchClients());
  }

  fetchCountries() {
    this.store.dispatch(new FetchCountries());
  }

  saveClient(client: Client) {
    if (client.id) {
      this.store.dispatch(new UpdateClient(client))
    } else {
      this.store.dispatch(new SaveClient(client));
    }
  }
}
