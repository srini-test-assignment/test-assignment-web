import {createFeatureSelector, createSelector} from "@ngrx/store";
import {CLIENT_FEATURE_KEY, ClientState} from "./client.reducer";

export const getClientState = createFeatureSelector<ClientState>(CLIENT_FEATURE_KEY);

export const getClient = createSelector(
  getClientState,
  (state: ClientState) => state.currentClient
);

export const getClients = createSelector(
  getClientState,
  (state: ClientState) => state.clients
);

export const getCountries = createSelector(
  getClientState,
  (state: ClientState) => state.countries
);

export const getLoaded = createSelector(
  getClientState,
  (state: ClientState) => state.loaded
);

