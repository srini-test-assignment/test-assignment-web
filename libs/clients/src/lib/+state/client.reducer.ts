import {Client, ClientListView, Country} from "@test-assignment-web/api";
import {ClientAction, ClientActionType} from "./client.actions";

export const CLIENT_FEATURE_KEY = 'client';

export interface ClientState {
  loaded: boolean;
  hasError: boolean;
  error?: string | null;
  clients: Array<ClientListView>;
  countries: Array<Country>;
  currentClient?: Client;
}

export interface ClientPartialState {
  readonly [CLIENT_FEATURE_KEY]: ClientState;
}

export const initialState: ClientState = {
  loaded: true,
  hasError: false,
  clients: [],
  countries: [],
}

export function reducer(
  state: ClientState = initialState,
  action: ClientAction
) {
  switch (action.type) {
    case ClientActionType.GetClient:
    case ClientActionType.FetchClients:
    case ClientActionType.FetchCountries:
      return {
        ...state,
        loaded: false
      };
    case ClientActionType.GetClientSuccess:
      return {
        ...state,
        loaded: true,
        currentClient: action.client
      };
    case ClientActionType.FetchClientsSuccess:
      return {
        ...state,
        loaded: true,
        clients: action.clients
      };
    case ClientActionType.FetchCountriesSuccess:
      return {
        ...state,
        loaded: true,
        countries: action.countries
      };
    case ClientActionType.RefreshClient:
      return {
        ...state,
        loaded: true,
        currentClient: null
      };
    default:
      return state;
  }
}
