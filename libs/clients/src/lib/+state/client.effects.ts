import {HttpClient} from "@angular/common/http";
import {DataPersistence} from "@nrwl/angular";
import {ClientPartialState} from "./client.reducer";
import {createEffect} from "@ngrx/effects";
import {
  ClientActionType,
  FetchClientsSuccess,
  FetchCountriesSuccess,
  GetClient,
  GetClientSuccess,
  SaveClient,
  SaveClientSuccess,
  UpdateClient,
  UpdateClientSuccess
} from "./client.actions";
import {HttpCommonService} from "@test-assignment-web/http-common";
import {map} from "rxjs";
import {Client, ClientListView, Country} from "@test-assignment-web/api";
import {Injectable} from "@angular/core";
import {Router} from "@angular/router";

@Injectable()
export class ClientEffects {

  private baseUrl = 'proxy/test-assignment/api'

  constructor(
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<ClientPartialState>,
    private httpCommonService: HttpCommonService,
    private router: Router,
  ) {}

  getClient$ = createEffect(() => this.dataPersistence.fetch(
    ClientActionType.GetClient,
    {
      run: (action: GetClient) => {
        return this.httpClient
          .get(
            this.baseUrl + '/client/' + action.clientId,
            { headers: this.httpCommonService.authorizedHeaders() }
          )
          .pipe(
            map(resp => {
              return new GetClientSuccess(resp as Client)
            })
          );
      }
    }
  ));

  fetchClients$ = createEffect(() => this.dataPersistence.fetch(
    ClientActionType.FetchClients,
    {
      run: () => {
        return this.httpClient
          .get(
            this.baseUrl + '/client',
            { headers: this.httpCommonService.authorizedHeaders() }
          )
          .pipe(
            map(resp => {
              return new FetchClientsSuccess(resp as Array<ClientListView>)
            })
          );
      }
    }
  ));

  fetchCountries$ = createEffect(() => this.dataPersistence.fetch(
    ClientActionType.FetchCountries,
    {
      run: () => {
        return this.httpClient
          .get(
            this.baseUrl + '/country',
            { headers: this.httpCommonService.authorizedHeaders() }
          )
          .pipe(
            map(resp => {
              return new FetchCountriesSuccess(resp as Array<Country>)
            })
          );
      }
    }
  ));

  saveClient$ = createEffect(() => this.dataPersistence.fetch(
    ClientActionType.SaveClient,
    {
      run: (action: SaveClient) => {
        return this.httpClient
          .post(
            this.baseUrl + '/client',
            JSON.stringify(action.client),
            { headers: this.httpCommonService.authorizedHeaders() }
          )
          .pipe(
            map(() => {
              this.router.navigate(['clients']);
              return new SaveClientSuccess()
            })
          );
      }
    }
  ));

  updateClient$ = createEffect(() => this.dataPersistence.fetch(
    ClientActionType.UpdateClient,
    {
      run: (action: UpdateClient) => {
        return this.httpClient
          .put(
            this.baseUrl + '/client/' + action.client.id,
            JSON.stringify(action.client),
            { headers: this.httpCommonService.authorizedHeaders() }
          )
          .pipe(
            map(() => {
              this.router.navigate(['clients']);
              return new UpdateClientSuccess()
            })
          );
      }
    }
  ));

}
