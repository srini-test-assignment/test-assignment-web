import {Action} from "@ngrx/store";
import {Client, ClientListView, Country} from "@test-assignment-web/api";

export enum ClientActionType {
  GetClient = '[GetClient] GetC Client',
  GetClientSuccess = '[GetClientSuccess] Get Client Success',
  FetchClients = '[FetchClients] Fetch Clients',
  FetchClientsSuccess = '[FetchClientsSuccess] Fetch Clients Success',
  FetchCountries = '[FetchCountries] Fetch Countries',
  FetchCountriesSuccess = '[FetchCountriesSuccess] Fetch Countries Success',
  SaveClient = '[SaveClient] Save Client',
  SaveClientSuccess = '[SaveClientSuccess] Save Client Success',
  UpdateClient = '[UpdateClient] Update Client',
  UpdateClientSuccess = '[UpdateClientSuccess] Update Client Success',
  RefreshClient = '[RefreshClient] Refresh Client',
}

export class GetClient implements Action {
  readonly type = ClientActionType.GetClient

  constructor(
    public clientId: number
  ) {}
}

export class GetClientSuccess implements Action {
  readonly type = ClientActionType.GetClientSuccess

  constructor(
    public client: Client
  ) {}
}

export class FetchClients implements Action {
  readonly type = ClientActionType.FetchClients
}

export class FetchClientsSuccess implements Action {
  readonly type = ClientActionType.FetchClientsSuccess

  constructor(
    public clients: Array<ClientListView>
  ) {}
}

export class FetchCountries implements Action {
  readonly type = ClientActionType.FetchCountries
}

export class FetchCountriesSuccess implements Action {
  readonly type = ClientActionType.FetchCountriesSuccess

  constructor(
    public countries: Array<Country>
  ) {}
}

export class SaveClient implements Action {
  readonly type = ClientActionType.SaveClient

  constructor(
    public client: Client
  ) {}
}

export class SaveClientSuccess implements Action {
  readonly type = ClientActionType.SaveClientSuccess
}

export class UpdateClient implements Action {
  readonly type = ClientActionType.UpdateClient

  constructor(
    public client: Client
  ) {}
}

export class UpdateClientSuccess implements Action {
  readonly type = ClientActionType.UpdateClientSuccess
}

export class RefreshClient implements Action {
  readonly type = ClientActionType.RefreshClient
}

export type ClientAction =
  | GetClient
  | GetClientSuccess
  | FetchClients
  | FetchClientsSuccess
  | FetchCountries
  | FetchCountriesSuccess
  | SaveClient
  | SaveClientSuccess
  | UpdateClient
  | UpdateClientSuccess
  | RefreshClient;
