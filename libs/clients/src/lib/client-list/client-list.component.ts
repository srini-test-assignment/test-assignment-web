import {Component, OnInit} from '@angular/core';
import {ClientFacede} from "../+state/client.facede";
import {ClientListView} from "@test-assignment-web/api";
import {Router} from "@angular/router";
import {ClientRoute} from "../client/client-route";

@Component({
  selector: 'test-assignment-web-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {

  clients: Array<ClientListView> = []
  clients$ = this.facade.clients$;
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'username', 'actions'];

  constructor(
    private facade: ClientFacede,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.clients$.pipe().subscribe(clients => this.clients = clients);
    this.facade.fetchClients();
  }

  addClient(): void {
    this.router.navigate([ClientRoute.newClient()]);
  }

  editClient(id: number): void {
    this.router.navigate([ClientRoute.client(id)]);
  }

  logout() {
    this.router.navigate(['login']);
  }
}
