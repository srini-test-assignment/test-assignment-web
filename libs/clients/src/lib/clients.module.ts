import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClientListComponent} from './client-list/client-list.component';
import {ClientFacede} from "./+state/client.facede";
import {StoreModule} from "@ngrx/store";
import {CLIENT_FEATURE_KEY, reducer} from "./+state/client.reducer";
import {ClientEffects} from "./+state/client.effects";
import {EffectsModule} from "@ngrx/effects";
import {HttpCommonService} from "@test-assignment-web/http-common";
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";
import {ClientComponent} from './client/client.component';
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(CLIENT_FEATURE_KEY, reducer),
    EffectsModule.forFeature([ClientEffects]),
    MatTableModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule
  ],
  declarations: [
    ClientListComponent,
    ClientComponent,
  ],
  exports: [ClientListComponent],
  providers: [ClientFacede, HttpCommonService]
})
export class ClientsModule {}
