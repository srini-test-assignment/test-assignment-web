import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ClientFacede} from "../+state/client.facede";
import {Client, Country} from "@test-assignment-web/api";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'test-assignment-web-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  countries: Array<Country> = []
  client: Client | undefined;

  constructor(
    private facade: ClientFacede,
    private activatedRoute: ActivatedRoute
  ) { }

  form: FormGroup = new FormGroup({
    firstName: new FormControl(),
    lastName: new FormControl(),
    username: new FormControl(),
    email: new FormControl(),
    address: new FormControl(),
    country: new FormControl(),
  });

  ngOnInit(): void {
    const clientId = this.activatedRoute.snapshot.params['clientId'];
    if (clientId) {
      this.facade.fetchClient(clientId);
    } else {
      this.facade.refreshClient();
    }
    this.facade.fetchCountries();
    this.facade.countries$.pipe().subscribe(countries => this.countries = countries);
    this.facade.client$.pipe().subscribe(client => {
      this.client = client
      this.refreshFormGroupFields();
    });
  }

  submit(): void {
    const client: Client = this.createclient(this.form.value);
    this.facade.saveClient(client);
  }

  refreshFormGroupFields() {
    if (this.client) {
      this.form.controls['firstName'].setValue(this.client.firstName);
      this.form.controls['lastName'].setValue(this.client.lastName);
      this.form.controls['username'].setValue(this.client.username);
      this.form.controls['email'].setValue(this.client.email);
      this.form.controls['address'].setValue(this.client.address);
      this.form.controls['country'].setValue(this.client.countryId);
    }
  }

  createclient(formValue: any): Client {
    return {
      id: this.client?.id,
      firstName: formValue.firstName,
      lastName: formValue.lastName,
      address: formValue.address,
      countryId: formValue.country,
      email: formValue.email,
      username: formValue.username
    }
  }
}
