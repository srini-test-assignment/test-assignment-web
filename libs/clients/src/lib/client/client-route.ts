export class ClientRoute {

  static newClient(): string {
    return 'clients/new';
  }

  static client(id: number): string {
    return 'clients/' + id;
  }
}
