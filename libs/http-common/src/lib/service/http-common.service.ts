import {Injectable} from "@angular/core";
import {HttpHeaders} from "@angular/common/http";
import {JwtService} from "@test-assignment-web/http-common";

@Injectable()
export class HttpCommonService {

  constructor(
    public jwtService: JwtService
  ) {
  }

  public headers(): HttpHeaders {
    console.log(this.jwtService.getJwt());
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    });
  }

  public authorizedHeaders(): HttpHeaders {
    if (this.jwtService.isJwtValid()) {
      return new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.jwtService.getJwt(),
      });
    } else {
      return this.headers();
    }
  }
}
