import {Injectable} from "@angular/core";
import {JwtHelperService} from "@auth0/angular-jwt";


@Injectable()
export class JwtService {

  private static tokenKey = 'srini-test-assignment-jwt';

  private jwtHelper = new JwtHelperService();

  public isJwtValid(): boolean {
    const jwt = this.getJwt();
    return jwt !== '' && !this.jwtHelper.isTokenExpired(jwt);
  }

  public saveJwt(jwt: string): void {
    console.log(jwt);
    localStorage.setItem(JwtService.tokenKey, jwt);
  }

  public getJwt(): string {
    return localStorage.getItem(JwtService.tokenKey) || '';
  }

  public removeJwt(): void {
    localStorage.removeItem(JwtService.tokenKey);
  }
}
