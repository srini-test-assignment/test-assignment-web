export interface ClientListView {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
}
