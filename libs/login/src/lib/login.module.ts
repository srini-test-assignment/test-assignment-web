import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {LoginEffects} from './+state/login.effects';
import {LoginFacade} from "./+state/login.facade";
import {LOGIN_FEATURE_KEY, reducer} from "./+state/login.reducer";
import {HttpCommonService, JwtService} from "@test-assignment-web/http-common";
import {JwtHelperService} from "@auth0/angular-jwt";

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature(LOGIN_FEATURE_KEY, reducer),
    EffectsModule.forFeature([LoginEffects]),
  ],
  declarations: [LoginComponent],
  exports: [LoginComponent],
  providers: [JwtService, JwtHelperService, LoginFacade, HttpCommonService]
})
export class LoginModule {}
