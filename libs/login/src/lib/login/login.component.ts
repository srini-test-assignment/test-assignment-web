import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {LoginRequest} from "@test-assignment-web/api";
import {LoginFacade} from "../+state/login.facade";
import {JwtService} from "@test-assignment-web/http-common";

@Component({
  selector: 'test-assignment-web-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  error$ = this.facade.error$;

  constructor(
    private facade: LoginFacade,
    private jwtService: JwtService,
  ) {}

  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  ngOnInit() {
    this.jwtService.removeJwt();
    this.error$.pipe().subscribe(e => console.log(e));
  }

  submit() {
    if (this.form.valid) {
      const loginRequest: LoginRequest = {
        username: this.form.value.username,
        password: this.form.value.password
      }
      this.facade.login(loginRequest);
    }
  }

}
