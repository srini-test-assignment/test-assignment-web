import {Injectable} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {LoginPartialState} from "./login.reducer";
import {getLoginError, getLoginLoaded} from "./login.selectors";
import {LoginRequest} from "@test-assignment-web/api";
import {Login} from "./login.actions";

@Injectable({
  providedIn: 'root'
})
export class LoginFacade {

  loading$ = this.store.pipe(select(getLoginLoaded));
  error$ = this.store.pipe(select(getLoginError));

  constructor(private store: Store<LoginPartialState>) {}

  login(loginRequest: LoginRequest) {
    this.store.dispatch(new Login(loginRequest));
  }
}
