import {LoginAction, LoginActionType} from "./login.actions";

export const LOGIN_FEATURE_KEY = 'login';

export interface LoginState {
  loaded: boolean;
  hasError: boolean;
  error?: string | null;
}

export interface LoginPartialState {
  readonly [LOGIN_FEATURE_KEY]: LoginState;
}

export const initialState: LoginState = {
  loaded: false,
  hasError: false,
}

export function reducer(
  state: LoginState = initialState,
  action: LoginAction
) {
  switch (action.type) {
    case LoginActionType.Login:
      return {
        ...state,
        hasError: false,
        loaded: false
      };
    case LoginActionType.LoginSuccess:
      return {
        ...state,
        hasError: false,
        loaded: true
      };
    case LoginActionType.LoginFailure:
      return {
        ...state,
        error: action.error,
        hasError: true,
        loaded: true
      };
    default:
      return state;
  }
}
