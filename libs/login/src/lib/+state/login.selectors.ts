import {createFeatureSelector, createSelector} from '@ngrx/store';
import {LOGIN_FEATURE_KEY, LoginState} from './login.reducer';

// Lookup the 'Login' feature state managed by NgRx
export const getLoginState = createFeatureSelector<LoginState>(LOGIN_FEATURE_KEY);

export const getLoginLoaded = createSelector(
  getLoginState,
  (state: LoginState) => state.loaded
);

export const getLoginError = createSelector(
  getLoginState,
  (state: LoginState) => state?.hasError ? state.error : ''
);
