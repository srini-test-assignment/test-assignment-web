import {Injectable} from '@angular/core';
import {DataPersistence} from "@nrwl/angular";
import {Login, LoginActionType, LoginFailure, LoginPartialState, LoginSuccess} from "@test-assignment-web/login";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {createEffect} from "@ngrx/effects";
import {HttpCommonService, JwtService} from "@test-assignment-web/http-common";
import {LoginResponse} from "@test-assignment-web/api";
import {map} from "rxjs";
import {Router} from "@angular/router";

@Injectable()
export class LoginEffects {

  private baseUrl = 'proxy/test-assignment/api/auth'

  constructor(
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<LoginPartialState>,
    private jwtService: JwtService,
    private router: Router,
    private httpCommonService: HttpCommonService,
  ) {}

  login$ = createEffect(() => this.dataPersistence.fetch(
    LoginActionType.Login,
    {
      run: (action: Login) => {
        return this.httpClient
          .post(
            this.baseUrl + '/login',
            JSON.stringify(action.loginRequest),
            { headers: this.httpCommonService.headers() }
          )
          .pipe(
            map(resp => {
              const response: LoginResponse = resp as LoginResponse;
              this.jwtService.saveJwt(response.jwt);
              this.router.navigate(['clients']);
              return new LoginSuccess();
            })
          )
      },
      onError: (action: Login, error: HttpErrorResponse) => {
        return new LoginFailure(error.error);
      }
    }
  ));

}
