import {Action} from '@ngrx/store';
import {LoginRequest} from "@test-assignment-web/api";

export enum LoginActionType {
  Login = '[Login] perform login',
  LoginSuccess = '[Login/API] Login Success',
  LoginFailure = '[Login/API] Login Failure',
}

export class Login implements Action {
  readonly type = LoginActionType.Login;

  constructor(
    public loginRequest: LoginRequest
  ) {}
}

export class LoginSuccess implements Action {
  readonly type = LoginActionType.LoginSuccess;
}

export class LoginFailure implements Action {
  readonly type = LoginActionType.LoginFailure;

  constructor(
    public error: string
  ) {}
}

export type LoginAction =
  | Login
  | LoginSuccess
  | LoginFailure
