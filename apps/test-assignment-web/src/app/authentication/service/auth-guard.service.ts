import {CanActivate, Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {JwtService} from "@test-assignment-web/http-common";

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(
    private jwtService: JwtService,
    private router: Router
  ) {}

  canActivate(): boolean {
    console.log('TESTING');
    if (!this.jwtService.isJwtValid()) {
      console.log('INVALID');
      console.log('INVALID');
      console.log('INVALID');
      console.log('INVALID');
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }

}
