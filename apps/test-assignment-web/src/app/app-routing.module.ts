import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AuthGuardService as AuthGuard} from "./authentication/service/auth-guard.service";
import {LoginComponent} from "@test-assignment-web/login";
import {ClientComponent, ClientListComponent} from "@test-assignment-web/clients";

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'clients',
    component: ClientListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'clients/new',
    component: ClientComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'clients/:clientId',
    component: ClientComponent,
    canActivate: [AuthGuard],
  },
  { path: '**', redirectTo: 'login' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {}
