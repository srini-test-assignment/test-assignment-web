import {Component} from '@angular/core';

@Component({
  selector: 'test-assignment-web-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'test-assignment-web';
  isIframe = false;
}
