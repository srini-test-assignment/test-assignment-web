import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {NxModule} from "@nrwl/angular";

import {AppComponent} from './app.component';
import {LoginModule} from "@test-assignment-web/login";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ClientsComponent} from "./clients/clients.component";
import {EffectsModule} from "@ngrx/effects";
import {StoreModule} from "@ngrx/store";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {environment} from "../environments/environment";
import {HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from "./app-routing.module";
import {ClientsModule} from "@test-assignment-web/clients";

@NgModule({
  declarations: [
    AppComponent,
    ClientsComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    LoginModule,
    ClientsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NxModule.forRoot(),
    StoreModule.forRoot( {}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
  ],

  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
